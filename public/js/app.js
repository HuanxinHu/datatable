class Table {
	constructor(params) {		
		this.dataStore = clientData;
		this.filterData = null;
		this.toggleHead = {};
		this.pageCounts = 10;
		this.pageIndex = 1;
		this.orderByValue = 'ID';
		this.order = 'INC',
		this.data200Url = 'data/data200.json';
	}
	getData(url) {
		return new Promise(function(resolve,reject){
			const request = new XMLHttpRequest();
			request.open('GET',url,true);
			request.onload = e => {
				if(request.status >= 200 && request.status < 400){
					const data = JSON.parse(request.responseText);
					resolve(data);
				}else{
					reject(e)
				}
			};
			request.onerror = e => {
				reject(e);
			}
			request.send();
		})
	}
	initToggleHead(dataItem) {
		let toggleHeadHTML=[];
		for(let key in dataItem){
			this.toggleHead[key] = true;

			let head = `<a data-head=${key}>${key}</a>`;
			toggleHeadHTML.push(head);
		}
		let panelHTML = `
			<div class='panel'>
				<div class='toggle-col'>Toggle column: 
					<span id='toggle-val'>
						${toggleHeadHTML.join(' - ')}
					</span> 
				</div>
				<div class="search">
					<span >load: <input id='dataNum' type="number" min=0 value=100 /> data</span>
				   | Search: <input type="text" id='search' name='search' placeholder='search'>
				</div>
			</div>	
		`.trim();
		return panelHTML;		
	}
	updateThead() {
		this.tbHeadContainer.innerHTML = this.renderThead();
	}
	renderThead() {
		let theadArray = [];
		for(let key in this.toggleHead){
			if(this.toggleHead[key]){
				let theadItem =	`
				    <th class='th-val' onclick='this.handleToggleSort("${key}")''>
				    	<span>${key}</span>
				    	<span class='th-panel'>
				    		<i class="${this.order=='INC'?'active':''} ${key==this.orderByValue?'selected':''} up fa fa-caret-up"></i>
				    		<i class="${this.order=='DEC'?'active':''} ${key==this.orderByValue?'selected':''} down fa fa-caret-down"></i>
				    	</span>
				    </th>
				`.trim();
				theadArray.push(theadItem);
			}
		}
		return `<tr>${theadArray.join('')}</tr>`;
	}
	updateTbody(data) {
		let tableBody = this.renderTbody(data,this.pageIndex,this.pageCounts);
		this.tbBodyContainer.innerHTML = tableBody;
	}
	renderTbody(data,pageIndex,pageCounts) {
		let tbodyArray = [];
		for(let i=(pageIndex-1)*pageCounts; i<pageIndex*pageCounts&&i<data.length; i++){
			let row = [];
			let dataItem = data[i];
			for(let key in this.toggleHead){
				if(this.toggleHead[key]){
					let tbItem = `<td>${dataItem[key]}</td>`;
					row.push(tbItem);
				}
			}
			tbodyArray.push(`<tr>${row.join('')}</tr>`);
		}
		return tbodyArray.join('');
	}
	updatePages(data) {
		let pages = this.renderPages(data.length,this.pageCounts);
		this.pagesContainer.innerHTML = pages;
	}
	renderPages(dataLength,pageCounts) {
		let pageArray = [];
		for(let i=1;i<=Math.ceil(dataLength/pageCounts); i++){
			let pageItem = `<a data-page-index=${i} class=${i==this.pageIndex?'page-active':''}>${i}</a>`;
			pageArray.push(pageItem);
		}
		return pageArray.join('');
	}
	render(data) {
		let togglePanel = this.initToggleHead(data[0]);
		let tableHead = this.renderThead();
		let tableBody = this.renderTbody(data,this.pageIndex,this.pageCounts);
		let pages = this.renderPages(data.length,this.pageCounts);
		let container = togglePanel +
			`<table>
				<thead>
					${tableHead}
				</thead>
				<tbody>
					${tableBody}
				</tbody>
			</table>
			<div class='page-area'>
				<div class="page-number">
					${pages}
				</div>
			</div>
			`.trim();
		document.getElementById('container').innerHTML = container;
		this.tbHeadContainer = document.getElementsByTagName('thead')[0];
		this.tbBodyContainer = document.getElementsByTagName('tbody')[0];
		this.pagesContainer  = document.getElementsByClassName('page-number')[0];
		this.bindEvents();
	}
	handleSearch() {
		let search = document.getElementById('search');
		search.addEventListener('input',()=>{
			let inputVal = search.value.toLowerCase();
			console.log(inputVal);
			this.filterData = this.dataStore.filter(item => {
				for( let key in item){
					if(item[key].toString().toLowerCase().indexOf(inputVal) > -1){
						return true;
					}
				}
				return false;
			});
			this.pageIndex = 1;
			this.updatePages(this.filterData);
			this.updateTbody(this.filterData);
		});
	}
	handleDataNumChange() {
		let dataNum = document.getElementById('dataNum');
		const privateFun = () => {
			this.pageIndex = 1;
			this.filterData = this.dataStore;
			this.updatePages(this.filterData);
			this.updateTbody(this.filterData);			
		};
		dataNum.addEventListener('input', () => {
			let inputVal = dataNum.value;
			console.log(inputVal);
			if(inputVal<=100){
				this.dataStore = clientData.slice(0,inputVal);
				this.pageCounts = 10;
				privateFun();
			}else{
				this.getData(this.data200Url).then(data => {
					this.dataStore = data.slice(0,inputVal);
					this.pageCounts = 20;
					privateFun();
				}).catch(err => {
					console.log(err);
				});
			}
		});
	}
	handlePageChange() {
		this.pagesContainer.addEventListener('click', e => {
			let target = e.target;
			console.log('chage page index: '+target.getAttribute('data-page-index'));
			
			this.pageIndex = target.getAttribute('data-page-index');

			this.updateTbody(this.filterData);
			this.updatePages(this.filterData);

		});
	}
	handleToggleHead() {
		let toggleA = document.querySelectorAll('#toggle-val a');
		for(let i=0; i<toggleA.length; i++){

			toggleA[i].addEventListener('click', e => {
				let targetVal = e.target.innerHTML;
				this.toggleHead[targetVal] = !this.toggleHead[targetVal];
				this.updateThead();
				this.updateTbody(this.filterData);

			});
		}
	}
	handleToggleSort(orderByValue) {
		let order = {
			'INC' : 1,
			'DEC' : -1
		};

		if(this.orderByValue == orderByValue){
			this.order = this.order=='INC' ? 'DEC' : 'INC';
		}else{
			this.order = 'INC'
		}
		this.orderByValue = orderByValue;
		console.log(orderByValue,this.order);
		this.filterData.sort((a, b) => {
			let valA = !isNaN(a[orderByValue]) ? a[orderByValue] : a[orderByValue].toLowerCase(); 
			let valB = !isNaN(b[orderByValue]) ? b[orderByValue] : b[orderByValue].toLowerCase();
			if(valA > valB){
				return order[this.order];
			} 
			if(valA < valB){
				return -order[this.order];
			}
			return 0;
		});
		this.pageIndex = 1;
		this.updateThead();
		this.updateTbody(this.filterData);
		this.updatePages(this.filterData);
	}
	bindEvents() {
		this.handleSearch();
		this.handleDataNumChange();
		this.handlePageChange();
		this.handleToggleHead();
	}
	init() {
		this.render(this.dataStore);
		this.filterData = this.dataStore;
	}
};

let table = new Table();
table.init();